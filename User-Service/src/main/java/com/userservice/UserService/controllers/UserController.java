package com.userservice.UserService.controllers;

import com.userservice.UserService.dtos.OrderDTO;
import com.userservice.UserService.dtos.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO> getOrdersByUserId(@PathVariable final Long id){
        return userService.getOrdersByUserId(id);
    }

    @PostMapping("/save")
    public boolean saveUser(@RequestBody Map<String,String> data){
        return userService.saveUser(data);
    }
}
